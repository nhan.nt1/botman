<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property int $department_id
 * @property \Cake\I18n\FrozenDate $entered_date
 * @property int $position_id
 * @property \Cake\I18n\FrozenDate $created_date
 * @property \Cake\I18n\FrozenDate $updated_date
 * @property \Cake\I18n\FrozenDate|null $deleted_date
 *
 * @property \App\Model\Entity\Department $department
 * @property \App\Model\Entity\Position $position
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => true,
        'password' => true,
        'name' => true,
        'department_id' => true,
        'entered_date' => true,
        'position_id' => true,
        'created_date' => true,
        'updated_date' => true,
        'deleted_date' => true,
        'department' => true,
        'position' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];
    protected function _setPassword(string $password): ?string
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher())->hash($password);
        }
    }
}

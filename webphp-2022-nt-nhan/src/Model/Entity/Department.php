<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Department Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $note
 * @property int $department_leader_id
 * @property int $department_floor_num
 * @property \Cake\I18n\FrozenDate $created_date
 * @property \Cake\I18n\FrozenDate $updated_date
 * @property \Cake\I18n\FrozenDate|null $deleted_date
 *
 * @property \App\Model\Entity\User[] $user
 */
class Department extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'note' => true,
        'department_leader_id' => true,
        'department_floor_num' => true,
        'created_date' => true,
        'updated_date' => true,
        'deleted_date' => true,
        'user' => true,
    ];
}

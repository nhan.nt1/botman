<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use App\Libs\ConfigUtil;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Department Model
 *
 * @property \App\Model\Table\UserTable&\Cake\ORM\Association\BelongsTo $User
 * @property \App\Model\Table\UserTable&\Cake\ORM\Association\HasMany $User
 *
 * @method \App\Model\Entity\Department newEmptyEntity()
 * @method \App\Model\Entity\Department newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Department[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Department get($primaryKey, $options = [])
 * @method \App\Model\Entity\Department findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Department patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Department[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Department|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Department saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Department[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Department[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Department[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Department[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class DepartmentTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('department');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('User', [
            'foreignKey' => 'department_leader_id',
            'joinType' => 'LEFT',
        ]);
        $this->addBehavior('Common');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->add('name', 'max-length', [
                'rule' => function ($value, $context) {
                    $len = mb_strlen($value);
                    if ($len > 255) {
                        return ConfigUtil::getMessage("ECL002", ["Department Name", 255, $len]);
                    }
                    return true;
                }
            ])
            ->notEmptyString('name', ConfigUtil::getMessage("ECL001", ["Department Name"]));

        $validator
            ->scalar('note')
            ->allowEmptyString('note');

        $validator
            ->integer('department_leader_id', ConfigUtil::getMessage("ECL010", ["Department Leader Doctor"]))
            ->notEmptyString('department_leader_id', ConfigUtil::getMessage("ECL001", ["Department Leader Doctor"]));

        $validator
            ->integer('department_floor_num', ConfigUtil::getMessage("ECL010", ["Floor Number"]))
            ->notEmptyString('department_floor_num', ConfigUtil::getMessage("ECL001", ["Floor Number"]));


        $validator
            ->date('created_date')
            ->allowEmptyDate('created_date');

        $validator
            ->date('updated_date')
            ->allowEmptyDate('updated_date');

        $validator
            ->date('deleted_date')
            ->allowEmptyDate('deleted_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('department_leader_id', 'User'), ['errorField' => 'department_leader_id', 'message' => ConfigUtil::getMessage('ECL094', ['Department Leader Doctor'])]);
        return $rules;
    }
    /**
     * get data list Department
     *
     * @return \App\Model\Entity\Department|bool
     */
    public function getListDepartment()
    {
        $query = $this->find();
        $query
            ->order(['Department.id' => "asc"])
            ->contain('User', function (Query $q) {
                return $q
                    ->select(['User.name'])
                    ->where(['User.deleted_date IS' => null]);
            });
        return $query;
    }
    /**
     * get Department by id
     *
     * @param $id
     * @return \App\Model\Entity\Department|bool
     */
    public function getDepartmentById($id)
    {
        try {
            $result = $this->find()
                ->where(['id' => $id, 'deleted_date IS' => null])->first();
            return $result;
        } catch (\Exception $e) {
            return null;
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;
use Cake\Database\Expression\QueryExpression;

/**
 * User Model
 *
 * @property \App\Model\Table\DepartmentsTable&\Cake\ORM\Association\BelongsTo $Departments
 * @property \App\Model\Table\PositionsTable&\Cake\ORM\Association\BelongsTo $Positions
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class UserTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('user');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Department', [
            'foreignKey' => 'department_id',
            'joinType' => 'LEFT',
        ]);
        $this->addBehavior('Common');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->date('entered_date')
            ->requirePresence('entered_date', 'create')
            ->notEmptyDate('entered_date');

        $validator
            ->scalar('position_id')
            ->add('position_id', 'existsIn', [
                'rule' => function ($value, $context) {
                    if (!in_array($value, ['0', '1', '2', '3'])) {
                        return false;
                    }
                    return true;
                }
            ]);
            
        $validator
            ->date('created_date')
            ->allowEmptyDate('created_date');

        $validator
            ->date('updated_date')
            ->allowEmptyDate('updated_date');

        $validator
            ->date('deleted_date')
            ->allowEmptyDate('deleted_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('department_id', 'Department'), ['errorField' => 'department_id']);
        return $rules;
    }
    public function search($params)
    {
        $query = $this->find();
        //Get params
        $name = isset($params['name']) ? $params['name'] : '';
        $entered_date_from = isset($params['entered_date_from']) ? $params['entered_date_from'] : '';
        $entered_date_to = isset($params['entered_date_to']) ? $params['entered_date_to'] : '';
        //search by name
        if (strlen($name) > 0) {
            $query->andWhere(['User.name LIKE'   => '%' . $name . '%']);
        }
        //search by entered_date
        $conditions_date = [];
        if (strlen($entered_date_from) > 0) {
            $entered_date_from = date('Y-m-d', strtotime($entered_date_from));
            $conditions_date['AND'][] = [new QueryExpression("DATE_FORMAT(entered_date, '%Y-%m-%d') >= '$entered_date_from'")];
        }
        if (strlen($entered_date_to) > 0) {
            $entered_date_to = date('Y-m-d', strtotime($entered_date_to));
            $conditions_date['AND'][] = [new QueryExpression("DATE_FORMAT(entered_date, '%Y-%m-%d') <= '$entered_date_to'")];
        }
        $query->andWhere($conditions_date);
        //contain
        $query->contain('Department', function (Query $q) {
            return $q
                ->select(['Department.name'])
                ->where(['Department.deleted_date IS' => null]);
        });
        //sort follow id and search existing account
        $query = $query->where(['User.deleted_date IS' => null])->order(["User.name" => "asc", 'User.id' => "asc"]);
        return $query;
    }
    /**
     * get data User
     *
     * @param $id
     * @return \App\Model\Entity\User|bool
     */
    public function getUserById($id = null)
    {
        try {
            $result = $this->find()
                ->where([
                    'id' => $id,
                    'deleted_date IS NULL'
                ])->first();
            return $result;
        } catch (\Exception $e) {
            return null;
        }
    }
    /**
     * get data list Department
     *
     * @param $id
     * @return \App\Model\Entity\User|bool
     */
    public function getListDepartment()
    {
        try {
            $result = $this->Department->find('list')
                ->where([
                    'Department.deleted_date IS NULL'
                ])
                ->order(["Department.name" => "asc"])
                ->all();
            return $result;
        } catch (\Exception $e) {
            return null;
        }
    }
}

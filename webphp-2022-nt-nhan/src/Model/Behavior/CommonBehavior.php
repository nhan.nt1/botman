<?php

declare(strict_types=1);

namespace App\Model\Behavior;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\Utility\Text;
use Cake\Routing\Router;

class CommonBehavior extends Behavior
{

    public function beforeSave(EventInterface $event, EntityInterface $entity, ArrayObject $options)
    {
        if ($entity->deleted_date) {
            return true;
        }
        if ($entity->id) {
            $entity->set('updated_date', date("Y-m-d"));
        } else {
            $entity->set('created_date', date("Y-m-d"));
            $entity->set('updated_date', date("Y-m-d"));
        }
    }
    public function beforeDelete(EventInterface $event, Entity $entity, ArrayObject $options)
    {
        return true;
    }
}

<?php

namespace App\Libs;

class DateUtil
{

    /**
     * @param \DateTime $date
     * @param int $number
     * @return \DateTime
     */
    public static function modifyMonths($date, $number) {
        $modify = (new \DateTime())->setDate($date->format('Y'), $date->format('n'), 15);
        $month  = $modify->modify($number . ' month')->format('n');
        $modify->setDate($modify->format('Y'), $modify->format('n'), $date->format('d'));

        if ($month != $modify->format('n')) {
            $modify->modify('-1 month');
            $modify->setDate($modify->format('Y'), $modify->format('n'), $modify->format('t'));
        }

        return $modify;
    }

    /**
     * @param \DateTime $date
     * @param int $number
     * @param string $format
     * @return string Ym
     */
    public static function modifyMonthsReturnYm($date, $number, $format='Y/m') {
        return self::modifyMonths($date, $number)->format($format);
    }

    /**
     * @param $strDate
     * @param $format
     * @return bool
     */
    public static function isDateFormatValid($strDate, $format) {
        $dateObj = ValueUtil::strToDate($strDate, $format);
        if ($dateObj != null && ($dateObj instanceof \DateTime)) {
            return true;
        }
        return false;
    }

    /**
     * Validate a string with format
     * @param $dateStr
     * @param $format
     * @return bool
     */
    public static function validateDateTime($dateStr, $format = 'Y/m/d H:i:s') {
        $date = \DateTime::createFromFormat($format, $dateStr);
        return $date && ($date->format($format) === $dateStr);
    }

    /**
     * @return string
     */
    public static function getTimestamp() {
        $microtime = floatval(substr((string)microtime(), 1, 8));
        $rounded = round($microtime, 3);
        $milisecond = substr((string)$rounded, 2, strlen($rounded));
        return date("YmdHis") . $milisecond;
    }
    
    /**
     * @param $value
     * @return bool
     */
    public static function checkDateTime($value) {
        if (is_null($value)
            || DateUtil::validateDateTime($value, 'Y/m/d') || DateUtil::validateDateTime($value, 'Y/m/d H:m:s')) {
            return true;
        }
        return false;
    }

    /**
     * @param $value
     * @param $format
     * @return bool
     */
    public static function checkDate($value, $format = 'Y/m/d') {
        if (is_null($value)
            || DateUtil::validateDateTime($value, $format) || DateUtil::validateDateTime($value, 'Y/m/d H:m:s')) {
            return true;
        }
        return false;
    }

    /**
     * @param $string
     * @param string $format
     * @return string
     */
    public static function convertString2DateTimeString($string, $format = 'Y/m/d') {
        $result = '';
        if ($string == '' || is_null($string)) {
            return $result;
        }

        $date = \DateTime::createFromFormat('Y年m月d日', $string);
        if ($date != false) {
            $result = $date->format($format);
        }
        $date = \DateTime::createFromFormat('Y-m-d', $string);
        if ($date != false) {
            $result = $date->format($format);
        }
        $date = \DateTime::createFromFormat('Y/m/d', $string);
        if ($date != false) {
            $result = $date->format($format);
        }
        $date = \DateTime::createFromFormat('Ymd', $string);
        if ($date != false) {
            $result = $date->format($format);
        }
        return $result;
    }

    /**
     * Simple PHP age Calculator
     *
     * Calculate and returns age based on the date provided by the user.
     * @param   date of birth('Format:yyyy-mm-dd').
     * @return  age based on date of birth
     */
    public static function ageCalculator($datetime) {
        $datetime = self::convertString2DateTimeString($datetime, 'Y-m-d');
        if (!empty($datetime)) {
            $birthdate = new \DateTime($datetime);
            $today = new \DateTime('today');
            $age = $birthdate->diff($today)->y;
            return $age;
        }
        return '';
    }

    /**
     * Convert FrozenTime to String
     *
     * @param $frozentime
     * @param string $format
     * @return null
     */
    public static function convertFrozenTime2String($frozentime, $format = 'Y/m/d H:m:s') {
        if (is_object($frozentime) && get_class($frozentime) == 'Cake\I18n\FrozenTime') {
            return $frozentime->format($format);
        }
        return null;
    }

    /**
     * Convert 令和 date to date
     *
     * @param $date
     * @return  string
     */
    public static function convertReiwaDateToDate($date) {
        $year = substr($date, 0, -4);
        $reiwaYear = 2019; //令和
        if($year != 1) {
            $reiwaYear = 2019 + ($year - 1);
        }
        $month = substr($date, -4, 2);
        $date = substr($date, -2, 2);
        $reiwa = (string)$reiwaYear . "-" . $month . "-" . $date;
        return $reiwa;
    }
}
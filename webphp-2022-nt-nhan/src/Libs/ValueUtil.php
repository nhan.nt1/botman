<?php

namespace App\Libs;

use Cake\I18n\FrozenTime;

class ValueUtil
{

    /**
     * Get value list from yml config file
     * @param $keys
     * @param array $options
     * @return array|null
     */
    public static function get($keys, $options = array()) {
        return ConfigUtil::getValueList($keys, $options);
    }

    /**
     * Get value list contain japanese and english
     * @param $keys
     * @param array $options
     * @return array|null
     */
    public static function getList($keys, $options = array()) {
        $options['getList'] = true;
        return ConfigUtil::getValueList($keys, $options);
    }

    /**
     * Convert from value into text in view
     *
     * @param $value property value Ex: 1
     * @param $listKey list defined in yml Ex: web.type
     * @return null|string text if exists else blank
     * @author sonPH
     */
    public static function valueToText($value, $listKey) {
        //Check params
        if(!isset($value) || !isset($listKey)){
            return NULL;
        }
        //Get list options
        $list = ValueUtil::get($listKey);
        if (empty($list)) {
            $list = ValueUtil::getList($listKey);
        }
        if(is_array($list) && isset($list[$value])){
            return h($list[$value]);
        }
        //Can't get value
        return NULL;
    }

    /**
     * Get value from const (in Yml config file)
     * @param $keys
     * @return int|null|string
     */
    public static function constToValue($keys) {
        return ConfigUtil::getValue($keys);
    }

    /**
     * Get text from const (in Yml config file)
     * @param $keys
     * @return int|null|string
     */
    public static function constToText($keys) {
        return ConfigUtil::getValue($keys, TRUE);
    }

    /**
     * Get value from test i
     * @param $searchText
     * @param $keys
     * @return int|null|string
     */
    public static function textToValue($searchText, $keys) {
        $valueList = ValueUtil::get($keys);
        foreach($valueList as $key => $text){
            if($searchText == $text){
                return $key;
            }
        }

        return NULL;
    }

    /**
     * @param $array
     * @param $key
     * @return array
     */
    public static function getArrayValue($array, $key) {
        $results = array();
        foreach($array as $data){
            $results[] = $data[$key];
        }

        return $results;
    }
    
    /**
     * Convert date to format Ymd
     * @param string $date
     * @return null|string
     */
    public static function dateToText($date) {
        if(empty($date)){
            return NULL;
        }
        
        if(!strtotime($date)){
            return $date;
        }
        
        $result = new \DateTime($date);
        return $result->format('Ymd');
    }
    
    /**
     * Create random string
     * @param int $length
     * @return null|string
     */
    public static function randomString($length) {
        $original_string = array_merge(range(0,9), range('a','z'), range('A', 'Z'));
        $original_string = implode("", $original_string);
        return substr(str_shuffle($original_string), 0, $length);
    }
    
    public static function getDateString($value,$format) {
        $value = str_replace(' ','',$value);
        $value = new \DateTime($value);
        return $value->format($format);
    }

    /**
     * @param $strDate
     * @param $strFormat base on format of \DateTime class like: Ymd etc
     * @return null|\DateTime
     */
    public static function strToDate($strDate, $strFormat) {
        $dateObj = null;
        if(!empty($strDate) && !empty($strFormat)){
            $dateObj = \DateTime::createFromFormat($strFormat, $strDate);
        }
        
        return $dateObj;
    }

    /**
     * Usefull to convert string of date from one format to other format
     *
     * @param $strDate
     * @param $srcFormat base on format of \DateTime class like: Ymd etc
     * @param $desFormat base on format of \DateTime class like: Ymd etc
     * @return string
     */
    public static function formatStrDate($strDate, $srcFormat, $desFormat) {
        $desStr = "";
        $dateObj = null;
        if(!empty($strDate) && !empty($srcFormat) && !empty($desFormat)){
            $dateObj = ValueUtil::strToDate($strDate, $srcFormat);
            if($dateObj != null){
                $desStr = $dateObj->format($desFormat);
            }
        }
        
        return $desStr;
    }
    
    /**
     * Convert all array keys to lower string
     * @param array $data
     * @return array
     */
    public static function lowerKeyArray($data) {
        $result = [];
        if(!empty($data) && is_array($data)){
            $result = array_change_key_case($data, CASE_LOWER);
        }
        
        return $result;
    }
    
    /**
     * Convert all array keys to upper string
     * @param array $data
     * @return array
     */
    public static function upperKeyArray($data) {
        $result = [];
        if(!empty($data) && is_array($data)){
            $result = array_change_key_case($data, CASE_UPPER);
        }
        
        return $result;
    }
    
    /**
     * Convert full width kana to half width kana or half width kana to full width kana
     * @param string $str
     * @param boolean $halfWidth
     * @return array
     */
    public static function convertKana($str, $halfWidth = true) {
        $result = "";
        $formatType = $halfWidth ? 'k' : 'K';
        if(!empty($str)){
            $result = mb_convert_kana($str, $formatType, 'UTF-8');
        }
        
        return $result;
    }

    public static function checkTelFormat($str) {
        $flag = false;
        $rex = "/^\\d{2}[0-9\\-]+[0-9]{1}$/u";
        $match = preg_match($rex, $str);
        if($match){
            $str = str_replace("-", "", $str);
            if(mb_strlen($str) >= 10 && mb_strlen($str) < 12){
                $flag = true;
            }
        }
        
        return $flag;
    }

    public static function checkMailFormat($str) {
        $flag = false;
        $rex = "/^[a-zA-Z0-9\\-\\._\\/\\?!%&~#\\$\\'\\*=\\^`\\{|\\}\\+]+@([a-z0-9\\-_]+\\.)+[a-z0-9\\-_]+$/u";
        if(preg_match($rex, $str)){
            $flag = true;
        }
        
        return $flag;
    }

    /**
     * Format String (YYYY/MM) to Date.YearMonth
     * Ex: 201906 to 2019/06 || 20190601 to 2019/06/01
     *
     * @param $str
     * @param bool $incudeDay
     * @return string
     */
    public static function formatStringToDate($str, $incudeDay = false) {
        $result = "";
        if(!empty($str)){
            $year = substr($str,0,4);
            $month = substr($str,4,2);
            $result = $year.'/'.$month;
            if ($incudeDay == true) {
                $day = substr($str, 6, 2);
                $result = $result. '/'. $day;
            }
        }

        return $result;
    }

    /**
     * Format Date.YearMonth to String (YYYY/MM)
     * Ex: 2019/06 to 201906
     *
     * @param $date
     */
    public static function formatDateToString($date) {
        $result = "";
        if(!empty($date)){
            $year = explode('/', $date)[0];
            $month = explode('/', $date)[1];
            $result = $year.$month;
        }

        return $result;
    }

    /**
     * Convert time object to string
     * @param $data
     * @param string $format
     * @return null
     */
    public static function formatDateTime($data, $format = 'Y/m/d') {
        if (empty($data) || is_null($data)) {
            return null;
        }
        return $data->format($format);
    }

    /**
     * @param $string
     * @param string $error
     * @return FrozenTime|null
     */
    public static function convert2FrozenTime($string) {
        if (empty($string) || is_null($string)) {
            return null;
        }
        return new FrozenTime($string);
    }

    /**
     * Format date
     *
     * @param string $date
     * @return string
     */
    public static function convertDate($date) {
        if(empty($date)){
            return '';
        }
        //Convert Y-m-d to Y/m/d
        if(strpos($date, '-') !== false){
            $date = str_replace('-', '/', $date);
            return h($date);
        }
        //Convert Y/m/d to Y-m-d
        if(strpos($date, '/') !== false){
            $date = str_replace('/', '-', $date);
            return h($date);
        }
    }
} 
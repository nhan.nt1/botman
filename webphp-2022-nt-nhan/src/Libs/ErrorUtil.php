<?php

namespace App\Libs;


class ErrorUtil
{
    //customer show errors
    public static function errors($errors = null, $message, $component)
    {
        if ($errors) {
            $errorMessages = [];
            array_walk_recursive($errors, function ($a) use (&$errorMessages) {
                $errorMessages[] = $a;
            });

            return $component->error(__($message), ['params' => ['errors' => $errorMessages]]);
        }
    }
}

<?php

namespace App\Libs;

use Cake\Log\Log;

class FileUtil
{
    /**
     * Write CSV file
     * 
     * @param string $url
     * @param string $content
     * @return bool
     */
    public static function writeCSV($url, $content)
    {
        $file = false;
        try {
            $file = fopen($url, "w");
            if ($file) {
                fwrite($file, $content);
            }
        } catch (\Exception $e) {
            Log::error("writeCSV: " . $e->getMessage());
        } finally {
            if ($file) {
                fclose($file);
            }
        }
    }

    /**
     * @param $fileName
     * @param array $header
     * @return array
     */
    public static function readCSV($fileName, $header = [])
    {
        $handle = fopen($fileName, "r");

        $fp = tmpfile();
        fwrite($fp, file_get_contents($fileName));
        rewind($fp);

        $result = [];

        @ini_set('memory_limit', '4096M');
        while ($data_file = fgetcsv($fp)) {
            if (!is_array($data_file)) continue;

            foreach ($data_file as $k => $df) {
                mb_convert_encoding($data_file[$k], "SJIS", 'UTF-8, SJIS, SJIS-WIN');
            }
            $info = array_combine($header, $data_file);
            $result[] = $info;
        }

        fclose($handle);
        $titles = array_shift($result);
        return [
            'titles' => $titles,
            'rows' => $result
        ];
    }


    /**
     * export csv
     * @param $data
     * @param $header
     * @param $headerDb
     * @return bool
     */
    public static function exportCSV($filename, $data, $header, $headerDb)
    {
        $encodeFunc = function ($value) {
            $value = str_replace('"', '""', $value);
            return '"' . $value . '"';
        };
        if (count($data) > 0) {
            $f = fopen('php://memory', 'w');
            fputs($f, implode(",", array_map($encodeFunc, $header)) . "\r\n");
            foreach ($data as  $row) {
                $lineData = [];
                foreach ($headerDb as $nameDb) {
                    if (str_contains($nameDb, 'department.name')) {
                        $lineData[] = isset($row->department->name) ? (string)$row->department->name : '';
                    } else {
                        if (strtotime($row->$nameDb) && strtotime($row->$nameDb) > 0) {
                            $lineData[] = date("Y/m/d", strtotime($row->$nameDb));
                        } else {
                            $lineData[] = (string)$row->$nameDb;
                        }
                    }
                }
                fputs($f, implode(",", array_map($encodeFunc, $lineData)) . "\r\n");
            }
            fseek($f, 0);
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            fpassthru($f);
        }
        return true;
    }
}

<?php

/**
 * Created by PhpStorm.
 * User: dat.tnt
 * Date: 2019/10/23
 * Time: 12:24
 */

namespace App\Controller;

use App\Libs\ConfigUtil;
use App\Libs\ErrorUtil;
use App\Libs\ValueUtil;
use Cake\Event\EventInterface;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Log content controller
 */
class LogController extends AppAdminController
{
    /**
     * Override beforeFilter callback
     *
     * @return \Cake\Network\Response|null|void
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['logout']);
    }

    /**
     * Override beforeRender callback
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(EventInterface $event)
    {
        parent::beforeRender($event);
        //Load layout
        $this->viewBuilder()->setLayout('login');
    }

    /**
     * Login to management system
     * 
     *
     * @return \Cake\Network\Response|null|void
     */
    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user && !$user['deleted_date']) {
                $this->Auth->setUser($user);
                return $this->redirect(['controller' => 'user', 'action' => 'list']);
            }
            $this->Flash->error(ConfigUtil::getMessage('ECL016'));
        }
    }


    /**
     * Logout function
     * @return \Cake\Http\Response|null
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Libs\FileUtil;
use App\Libs\ErrorUtil;
use App\Libs\ConfigUtil;
use Cake\Event\EventInterface;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserController extends AppAdminController
{
    /**
     * Override beforeFilter callback
     *
     * @return \Cake\Network\Response|null|void
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        //Load common model
        $this->loadModel('Department');
        //Load common component
        $this->loadComponent('Query');
    }
    /**
     * A-USE-01 User List
     * A-USE-01
     */
    public function list()
    {
        //read session
        $session = $this->request->getSession();
        //get data search
        $searchData = $this->getRequest()->getParam('?');
        try {
            //check searchData
            if (!$searchData) {
                $users = [];
                //delete session User.outputCSV
                $session->delete('User.outputCSV');
            } else {
                $result = $this->User->search($searchData);
                //Write session
                $session->write('User.outputCSV', json_encode($result));
                //write data to session for exportCSV
                isset($users) ? $users = [] : $users = $this->paginate($result, ['limit' => ConfigUtil::get('paginate.ten_pages')]);
            }
            //show error no record
            if (count($users) < 1 && $searchData ? count($searchData) > 0 : false) {
                $this->Flash->error('No Record');
            }
            $this->set(compact('users', 'searchData'));
        } catch (\Throwable $th) {
            $users = [];
            $this->Flash->error('No Record');
            $session->delete('User.outputCSV');
            $this->set(compact('users', 'searchData'));
        }
    }
    /**
     * A-USE-01 User List
     * A-USE-01
     */
    public function outputCSV()
    {
        $session = $this->request->getSession();
        //check permission
        if ($session->read('IsAccess')) {
            //Get data to export
            $result = $session->read('User.outputCSV');
            !isset($result) ? $result = "[]" : '';
            //check result
            if ($result !== "[]") {
                //excute export data
                $filename = 'list_user_' . date('YmdHis') . ".csv";
                $data = json_decode($result);
                $header = ['ID', 'User Name', 'Email', 'Department ID', 'Department Name', 'Entered Date', 'Position', 'Created Date', 'Updated Date'];
                $headerDb = ['id', 'name', 'email', 'department_id', 'department.name', 'entered_date', 'position_id', 'created_date', 'updated_date'];
                $result = FileUtil::exportCSV($filename, $data, $header, $headerDb);
                exit;
            }
            //show message no record
            $this->Flash->error('No Record');
            return $this->redirect(['action' => 'list']);
        }
    }
    /**
     * A-USE-02 UserAddEditDelete
     * A-USE-02
     */
    public function addeditdelete($id = "")
    {
        $user = $this->User->newEmptyEntity();
        //get list department
        $departments = $this->User->getListDepartment();
        //get value to check permission
        $position_id_auth = $this->Auth->user()['position_id'];
        $id_auth = $this->Auth->user()['id'];
        if (!empty($id)) {
            //get user by id not deleted
            $user = $this->User->getUserById($id);
            //check user exists
            if (!isset($user)) {
                return $this->redirect(['action' => 'addeditdelete']);
            }
            if ($position_id_auth !== 0 && $id != $id_auth) { #check permission
                return $this->redirect(['controller' => '', 'action' => 'logout']);
            }
        } else if (!isset($id) && $position_id_auth !== 0) { #check permission
            return $this->redirect(['controller' => '', 'action' => 'logout']);
        }
        $this->set(compact('departments', 'user'));
    }
    /**
     * A-USE-02 UserAddEditDelete
     * A-USE-02
     */
    public function add()
    {
        try {
            if ($this->request->is(['patch', 'post', 'put'])) {
                $params = $this->getRequest()->getData();
                $params = $this->processDataOnProScreen($params);
                $userExisted = $this->Query->checkEmailExisted('User', $params['email']);
                //check duplicate email
                if (!isset($userExisted)) {
                    $user = $this->Query->saveTable('User', $params);
                } else {
                    $this->Flash->error(ConfigUtil::getMessage('ECL019'));
                    return $this->redirect(['action' => 'addeditdelete']);
                }
                //check when save data success
                if (empty($user->getErrors())) {
                    return $this->redirect(['action' => 'list']);
                }
                //check error
                if ($user->getErrors()) {
                    $this->Flash->error(ConfigUtil::getMessage('ECL093'));
                    return $this->redirect(['action' => 'addeditdelete']);
                }
            }
        } catch (\Throwable $th) {
            return $this->redirect(['action' => 'addeditdelete']);
        }
    }
    /**
     * A-USE-02 UserAddEditDelete
     * A-USE-02
     */
    public function edit($id)
    {
        try {
            $position_id_auth = $this->Auth->user()['position_id'];
            //get user by id not deleted
            $user = $this->User->getUserById($id);
            //check method submit
            if ($this->request->is(['patch', 'post', 'put'])) {
                $params = $this->getRequest()->getData();
                //if user has role belongs [1 2 3] then just change the password
                if (isset($user) && in_array($position_id_auth, [1, 2, 3])) {
                    $password = $params['password'];
                    $params = [];
                    $params['password'] = $password;
                }
                $params = $this->processDataOnProScreen($params);
                $userExisted = $this->Query->checkEmailExisted('User', $params['email'], $user->id);
                //check duplicate email
                if (!isset($userExisted)) {
                    $user = $this->Query->saveTable('User', $params, $id);
                } else {
                    $this->Flash->error(ConfigUtil::getMessage('ECL019'));
                    return $this->redirect(['action' => 'addeditdelete', $id]);
                }
                //check when save data success
                if (empty($user->getErrors())) {
                    return $this->redirect(['action' => 'list']);
                }
                //check error
                if ($user->getErrors()) {
                    $this->Flash->error(ConfigUtil::getMessage('ECL093'));
                    return $this->redirect(['action' => 'addeditdelete', $id]);
                }
            }
        } catch (\Throwable $th) {
            return $this->redirect(['action' => 'addeditdelete', $id]);
        }
    }
    /**
     * A-USE-02 UserAddEditDelete
     * A-USE-02
     */
    public function delete($id)
    {

        $id_auth = $this->Auth->user()['id'];
        //check when remove user logged in
        if ($id_auth == $id) {
            $this->Flash->error(ConfigUtil::getMessage('ECL086'));
            return $this->redirect(['action' => 'addeditdelete', $id]);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->User->getUserById($id);
            if (isset($user)) {
                $params['deleted_date'] = date('Y-m-d');
                $user = $this->Query->saveTable('User', $params, $user->id);
            } else {
                $this->Flash->error(ConfigUtil::getMessage('ECL093'));
                return $this->redirect(['action' => 'addeditdelete', $id]);
            }
        }
        //check when save data success
        return $this->redirect(['action' => 'list']);
    }
    /**
     * process data enter on screen
     * @return array
     */
    public function processDataOnProScreen($params)
    {
        if (!empty($params['entered_date'])) {
            $params['entered_date'] = date('Y-m-d', strtotime($params['entered_date']));
        }
        //process password
        if (empty($params['password'])) {
            unset($params['password']);
        }
        return $params;
    }
}

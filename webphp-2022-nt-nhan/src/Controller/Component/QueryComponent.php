<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;

class QueryComponent extends Component
{
    /**
     * check email exists when registration
     * @param string $table
     * @param $email
     * @param null $id
     * @return null
     */
    public function checkEmailExisted($table, $email, $id = null)
    {
        $table = TableRegistry::getTableLocator()->get($table);
        try {
            $id = !empty($id) ? $id : '';
            $result = $table->find()
                ->where([
                    'email' => $email,
                    'deleted_date IS NULL'
                ]);
            if (!empty($id)) {
                $result->andWhere(['id <>' => $id]);
            }
            return $result->first();
        } catch (\Exception $ex) {
            return null;
        }
    }
    /**
     * save data to table
     *
     * @return object
     */
    public function saveTable($table, $params, $id = null)
    {
        $table = TableRegistry::getTableLocator()->get($table);
        try {
            if (empty($id)) {
                $entity = $table->newEntity([]);
            } else {
                $entity = $table->get($id);
            }
            $object = $table->patchEntity($entity, $params);
            $table->getConnection()->begin();
            $result = $table->save($object);
            if ($result) {
                $table->getConnection()->commit();
            } else {
                $table->getConnection()->rollback();
            }
            return $object;
        } catch (\Exception $e) {
            $table->getConnection()->rollback();
            Log::error($e);
            return false;
        }
    }
    /**
     * save data to table for import
     *
     * @return object
     */
    public function saveTableForImport($table, $params, $id = null)
    {
        $table = TableRegistry::getTableLocator()->get($table);
        try {
            if (empty($id)) {
                $entity = $table->newEntity([]);
            } else {
                $entity = $table->get($id);
            }
            $object = $table->patchEntity($entity, $params);
            $table->save($object);
            return $object;
        } catch (\Exception $e) {
            Log::error($e);
            return false;
        }
    }
}

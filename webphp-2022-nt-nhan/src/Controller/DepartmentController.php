<?php

declare(strict_types=1);

namespace App\Controller;

use App\Libs\ErrorUtil;
use App\Libs\ConfigUtil;
use Cake\Event\EventInterface;

/**
 * Departments Controller
 *
 * @property \App\Model\Table\DepartmentsTable $Departments
 * @method \App\Model\Entity\Department[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DepartmentController extends AppAdminController
{
    /**
     * Override beforeFilter callback
     *
     * @return \Cake\Network\Response|null|void
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        //Load common component
        $this->loadComponent('Query');
    }
    /**
     * A-DEP-01 Department List
     * A-DEP-01
     */
    public function list()
    {
        // importcsv
        if ($this->request->is(['patch', 'post', 'put'])) {
            $header = ['department.id', 'department.name', 'department.note', 'department.department_leader_id', 'department.department_floor_num', 'delete'];
            $filename = $_FILES["fileCSV"]["tmp_name"];
            $handle = fopen($filename, "r");
            $headerS = fgetcsv($handle);
            $this->importcsv($_FILES["fileCSV"], $handle, $header);
        }
        //Get records from database
        $result = $this->Department->getListDepartment();
        $departments = $this->paginate($result, ['limit' => ConfigUtil::get('paginate.fifteen_pages')]);
        //Show list evaluation
        $this->set(compact('departments'));
    }
    /**
     * A-DEP-01 Department List
     * A-DEP-01
     */
    public function importcsv($file, $handle, $header)
    {
        if ($file["size"] > 0) {
            // read each data row in the file
            $i = 0;
            $this->connection->begin();
            $htmlErrors = [];
            while (($row = fgetcsv($handle)) !== FALSE) {
                $i++;
                $result = $this->Department->newEmptyEntity();
                $data = $this->processDataInFile($header, $row);
                // we have an row contain delete whose value is Y, so we will delete department
                if ($data['department']['delete'] === 'Y') {
                    $department = $this->Department->getDepartmentById($data['department']['id']);
                    if (isset($department)) {
                        $params['deleted_date'] = date('Y-m-d');
                        $result = $this->Query->saveTableForImport('Department', $params, $department->id);
                    } else {
                        $result->setErrors(['NotFound' => ConfigUtil::getMessage('ECL094', ['ID'])]);
                    }
                }
                // add or edit
                else {
                    if (empty($data['department']['id'])) { #add department
                        $result = $this->Query->saveTableForImport('Department', $data['department']);
                    } else { #edit department
                        $department = $this->Department->getDepartmentById($data['department']['id']);
                        if (isset($department)) {
                            $result = $this->Query->saveTableForImport('Department', $data['department'], $data['department']['id']);
                        } else {
                            $result->setErrors(['NotFound' => ConfigUtil::getMessage('ECL094', ['ID'])]);
                        }
                    }
                }
                //add errors to htmlErrors
                if ($result->getErrors()) {
                    foreach ($result->getErrors() as $item) {
                        foreach ($item as $value) {
                            $htmlErrors[$i+1][] = 'Dòng: ' . $i+1 . $value;
                        }
                    }
                }
            }
            if ($htmlErrors !== []) { #show errors
                $this->connection->rollback();
                ErrorUtil::errors($htmlErrors, '', $this->Flash);
                return $this->redirect(['action' => 'list']);
            } else { #import success
                $this->connection->commit();
                return $this->redirect(['action' => 'list']);
            }
        }
    }
    /**
     * process data enter on screen
     * @return array
     */
    public function processDataInFile($header, $row)
    {
        foreach ($header as $k => $head) {
            // get the data field from Model.field
            if (strpos($head, '.') !== false) {
                $h = explode('.', $head);
                $data[$h[0]][$h[1]] = (isset($row[$k])) ? $row[$k] : '';
            }
            // get the data field from field
            else {
                $data['department'][$head] = (isset($row[$k])) ? $row[$k] : '';
            }
        }
        return $data;
    }
}

<?php

declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use App\Libs\ConfigUtil;
use Cake\Event\EventInterface;
use Cake\Controller\Controller;
use Cake\Datasource\ConnectionManager;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 */
class AppAdminController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ],
                    'userModel' => 'User',
                ]
            ],
            'loginAction' => [
                'controller' => 'Log',
                'action' => 'login'
            ],
            'loginRedirect' => array('controller' => 'User', 'action' => 'list'),
            'logoutRedirect' => array('controller' => 'Log', 'action' => 'login'),
            'authError' => '',
        ]);
        //get id,name,role user login and read session
        $position_id_auth = isset($this->Auth->user()['position_id']) ? $this->Auth->user()['position_id'] : '';
        $name_auth = isset($this->Auth->user()['name']) ? $this->Auth->user()['name'] : '';
        $id_auth = isset($this->Auth->user()['id']) ? $this->Auth->user()['id'] : '';
        //get list role and get role name
        $roleList = ConfigUtil::get('role');
        $roleNameAuth = isset($roleList[$position_id_auth]) ? $roleList[$position_id_auth] : '';
        //Set for all Views
        $session = $this->getRequest()->getSession();
        $this->set(compact('session', 'name_auth', 'roleNameAuth', 'id_auth'));
        //Set connection
        $this->connection = ConnectionManager::get('default');

    }
    public function beforeRender(EventInterface $event)
    {
        parent::beforeRender($event);
        $this->viewBuilder()->setLayout('admin');
    }
    public function isAuthorized($user)
    {
        //get session
        $session = $this->getRequest()->getSession();
        // Get controller and action parameter
        $controller = $this->getRequest()->getParam('controller');
        $action = $this->getRequest()->getParam('action');
        // Get firewall setting
        $firewallList = ConfigUtil::get('firewall');
        if ($user === null || !array_key_exists('position_id', $user)) {
            $firewall  = $firewallList['DEFAULT'];
        } else {
            $role = $this->getRole($user);
            $firewall = $firewallList[$role];
        }
        //Check screen permission
        if ($firewall && array_key_exists($controller, $firewall)) {
            if ($firewall[$controller] === null || in_array($action, $firewall[$controller])) {
                $session->write('IsAccess', true);
                return true;
            }
        }
        //write session
        $session->write('IsAccess', false);
        //If can't go to page, redirect to error page
        return $this->redirect(['controller' => 'Top', 'action' => 'permissionError']);
    }
    protected function getRole($user) {
        if(!$user){
            return null;
        }
        //Get list of roles
        $roleList = ConfigUtil::get('role');
        if(empty($roleList[$user['position_id']])){
            return null;
        } else {
            return $roleList[$user['position_id']];
        }
    }

    function xssClean($data)
    {
        // Fix &entity\n;
        $data = str_replace(array('&amp;', '&lt;', '&gt;'), array('&amp;amp;', '&amp;lt;', '&amp;gt;'), $data);
        $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
        $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
        $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');
        // Remove any attribute starting with "on" or xmlns
        $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);
        // Remove javascript: and vbscript: protocols
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);
        // Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);
        // Remove namespaced elements (we do not need them)
        $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);
        do {
            // Remove really unwanted tags
            $old_data = $data;
            $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
        } while ($old_data !== $data);
        return $data;
    }

    /**
     * Process to break link in html string
     *
     * @param string $html
     * @return string
     */
    public function breakLine($html)
    {
        return str_replace('<br />', "\n", $html);
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

/**
 * Top Controller
 *
 * @method \App\Model\Entity\Top[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TopController extends AppController
{
    public function permissionError()
    {
        return $this->redirect(['controller' => '', 'action' => 'logout']);
    }
}

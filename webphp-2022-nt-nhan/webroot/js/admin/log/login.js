$(document).ready(function () {
    //Validation data using JS
    $("#formLogin").validate({
        focusInvalid: true,
        rules: {
            email: {
                required: true,
                email: true,
            },
            password: {
                required: true,
            },
        },
        messages: {
            email: {
                required: function (params, input) {
                    return $.validator.messages.required(
                        $(input).data("label")
                    );
                },
            },
            password: {
                required: function (params, input) {
                    return $.validator.messages.required(
                        $(input).data("label")
                    );
                },
            },
        },
        submitHandler: function(form) {
            $('.btn-login').attr('disabled', true);
            form.submit();
        }
    });
});

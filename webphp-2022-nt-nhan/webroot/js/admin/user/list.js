$(document).ready(function () {
    // format date
    $("#entered_date_from").datepicker({
        dateFormat: "yy/mm/dd",
        changeMonth: true,
        changeYear: true,
        onClose: function (e) {
            $(this).valid();
        },
    });
    $("#entered_date_to").datepicker({
        dateFormat: "yy/mm/dd",
        changeMonth: true,
        changeYear: true,
    });
    //check validate
    $("#formSearchUser").validate({
        focusInvalid: true,
        rules: {
            entered_date_from: {
                greaterThan: "#entered_date_to",
            },
        },
        messages: {
            entered_date_from: {
                greaterThan: $.validator.messages.greaterThan(),
            },
        },
        submitHandler: function(form) {
            $('.btn-search-user').attr('disabled', true);
            form.submit();
        }
    });
    //change href /user/list to /user/list?page=1
    $('a[href="/user/list"]').attr("href", "/user/list?page=1");
    //clear data in form
    $(".btnClear").click(function (e) {
        $(":input")
            .not("#accountType")
            .each(function () {
                if (this.type == "text" || this.type == "textarea") {
                    this.value = "";
                } else if (this.type == "radio" || this.type == "checkbox") {
                    this.checked = false;
                } else if (
                    this.type == "select-one" ||
                    this.type == "select-multiple"
                ) {
                    this.value = "All";
                }
            });
    });
    //remove 2 button 前へ, 次へ when only 1 page
    if ($('a.page-link').length == 2) {
        $('a.page-link').attr('style', 'display:none')
    }
});

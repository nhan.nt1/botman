$(document).ready(function() {
    // format date
    $('input[name="entered_date"]').datepicker({
        dateFormat: "yy/mm/dd",
        changeMonth: true,
        changeYear: true,
        onClose: function(e) {
            $(this).valid();
        },
    });
    //check delete user
    $('.btn-delete-user').click(function() {
        let id_user = $('#ip_id_user').val()
        let id_auth = $('#ip_id_auth').val()
        if (id_user !== id_auth) {
            if (confirm("このユーザーを削除しますか？")) {
                $('.btn-delete-user').attr('disabled', true);
                $('#formDeleteUser').submit();
            };
        } else {
            $('.error-message').html($.validator.messages.errorRemoveAuth())
        }
    });
    //check validate
    $("#formAddEditDeleteUser").validate({
        focusInvalid: true,
        rules: {
            name: {
                required: true,
                maxlength: 50,
            },
            email: {
                required: true,
                maxlength: 255,
                email: true,
            },
            department_id: {
                required: true,
            },
            entered_date: {
                required: true,
                formatDate: true
            },
            position_id: {
                required: true,
            },
            password: {
                requiredPassword: '#ip_id_user',
                maxlength: 20,
                passwordLength: true,
                passwordRange: true
            },
            password_confirmation: {
                requiredPassword: '#ip_id_user',
                maxlength: 20,
                equalTo: '#password'
            },
        },
        messages: {
            name: {
                required: function(params, input) {
                    return $.validator.messages.required(
                        $(input).data("label")
                    );
                },
                maxlength: function(params, input) {
                    return $.validator.messages.maxlength([
                        $(input).data("label"),
                        params,
                        $(input).val().length,
                    ]);
                },
            },
            email: {
                required: function(params, input) {
                    return $.validator.messages.required(
                        $(input).data("label")
                    );
                },
                maxlength: function(params, input) {
                    return $.validator.messages.maxlength([
                        $(input).data("label"),
                        params,
                        $(input).val().length,
                    ]);
                },
            },
            department_id: {
                required: function(params, input) {
                    return $.validator.messages.required(
                        $(input).data("label")
                    );
                },
            },
            entered_date: {
                required: function(params, input) {
                    return $.validator.messages.required(
                        $(input).data("label")
                    );
                },
                formatDate: function(params, input) {
                    return $.validator.messages.date(
                        $(input).data("label")
                    );
                },
            },
            position_id: {
                required: function(params, input) {
                    return $.validator.messages.required(
                        $(input).data("label")
                    );
                },
            },
            password: {
                requiredPassword: function(params, input) {
                    return $.validator.messages.requiredPassword(
                        $(input).data("label")
                    );
                },
                maxlength: function(params, input) {
                    return $.validator.messages.maxlength([
                        $(input).data("label"),
                        params,
                        $(input).val().length,
                    ]);
                },
                passwordRange: function(params, input) {
                    return $.validator.messages.passwordRange(
                        $(input).data("label")
                    );
                },
            },
            password_confirmation: {
                requiredPassword: function(params, input) {
                    return $.validator.messages.requiredPassword(
                        $(input).data("label")
                    );
                },
                maxlength: function(params, input) {
                    return $.validator.messages.maxlength([
                        $(input).data("label"),
                        params,
                        $(input).val().length,
                    ]);
                },
                equalTo: function(params, input) {
                    return $.validator.messages.equalTo();
                },
            }
        },
        submitHandler: function(form) {
            $('.btn-save-user').attr('disabled', true);
            form.submit();
        }
    });
});
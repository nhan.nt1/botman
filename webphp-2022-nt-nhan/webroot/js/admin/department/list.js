$(document).ready(function () {
    //check validate
    $("#formImportCSV").validate({
        submitHandler: function(form) {
            $('.btn-importcsv').attr('disabled', true);
            form.submit();
        }
    });
    //remove 2 button 前へ, 次へ when only 1 page
    if ($('a.page-link').length == 2) {
        $('a.page-link').attr('style', 'display:none')
    }

})
$.validator.addMethod("greaterThan", function(value, element, params) {
    if ($(element).val() && $(params).val()) {
        let dateFrom = new Date(value);
        let dateTo = new Date($(params).val());
        return dateFrom.getTime() <= dateTo.getTime();
    }
    return true;
});
$.validator.addMethod("passwordLength", function(value, element, params) {
    return (
        this.optional(element) ||
        /^.{8,20}$/.test(
            value
        )
    );
});
$.validator.addMethod("passwordRange", function(value, element, params) {
    return (
        this.optional(element) ||
        /^[0-9a-z]*$/i.test(
            value
        )
    );
});
$.validator.addMethod("requiredPassword", function(value, element, params) {
    //if id exists then required password
    if ($(params).val().trim() === '' && value.trim() === '') {
        return false;
    }
    return true;
});
//check format date (mm/dd/yyyy)
$.validator.addMethod("formatDate", function(value, element, params) {
    if (!moment(value, 'MM/DD/YYYY', true).isValid()) {
        if (!moment(value, 'YYYY/MM/DD', true).isValid()) {
            return false;
        }
    }
    return true;
});
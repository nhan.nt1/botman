$.extend(jQuery.validator, {
    messages: {
        required: $.validator.format("{0}は必須項目です。"),
        requiredPassword: $.validator.format("{0}は必須項目です。"),
        email: $.validator.format("メールアドレスを正しく入力してください。"),
        maxlength: $.validator.format("{0}は「{1}」文字以下で入力してください。（現在{2}文字）"),
        greaterThan: $.validator.format("解約予定日は契約終了日前を指定してください。"),
        passwordLength: $.validator.format("パスワードは半角英数字記号で8～20文字で入力してください。"),
        passwordRange: $.validator.format("{0}は半角英数で入力してください。"),
        equalTo: $.validator.format("確認用のパスワードが間違っています。"),
        errorRemoveAuth: $.validator.format("すでに証明書番号は登録されています。"),
        date: $.validator.format("{0}は日付を正しく入力してください。"),
    }
});
$.validator.setDefaults({
    errorClass: 'error-message',
    errorElement: 'div',
    // add default behaviour for on focus out
    onfocusout: function(element) {
        this.element(element);
    }
});
$(function() {
    var _common = {};
    var _message = {
        'COMMON_ERROR': 'システムエラーが発生しました',
    };

    // bind to window variable, make it usable everywhere
    $.extend(window, {
        _common: _common,
        _message: _message,
    });

    /**
     * Check value is empty or not
     * @param string val
     * @returns boolean
     */
    function isEmpty(val) {
        return (val === undefined || val == null || val.length <= 0) ? true : false;
    }
    _common.isEmpty = isEmpty;

    //change link user list
    setTimeout(() => {
        $('li.nav-item a[href="/user/list?page=1"]').attr('href', '/user/list')
    }, 1);

});
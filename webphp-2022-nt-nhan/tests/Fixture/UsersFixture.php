<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 */
class UsersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'email' => 'Lorem ipsum dolor sit amet',
                'password' => 'Lorem ipsum dolor sit amet',
                'name' => 'Lorem ipsum dolor sit amet',
                'department_id' => 1,
                'entered_date' => '2022-03-30',
                'position_id' => 1,
                'created_date' => '2022-03-30',
                'updated_date' => '2022-03-30',
                'deleted_date' => '2022-03-30',
            ],
        ];
        parent::init();
    }
}

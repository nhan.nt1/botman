<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UserFixture
 */
class UserFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'user';
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'email' => 'Lorem ipsum dolor sit amet',
                'password' => 'Lorem ipsum dolor sit amet',
                'name' => 'Lorem ipsum dolor sit amet',
                'department_id' => 1,
                'entered_date' => '2022-03-31',
                'position_id' => 1,
                'created_date' => '2022-03-31',
                'updated_date' => '2022-03-31',
                'deleted_date' => '2022-03-31',
            ],
        ];
        parent::init();
    }
}

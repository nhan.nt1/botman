<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DepartmentFixture
 */
class DepartmentFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'department';
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'note' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'department_leader_id' => 1,
                'department_floor_num' => 1,
                'created_date' => '2022-04-01',
                'updated_date' => '2022-04-01',
                'deleted_date' => '2022-04-01',
            ],
        ];
        parent::init();
    }
}

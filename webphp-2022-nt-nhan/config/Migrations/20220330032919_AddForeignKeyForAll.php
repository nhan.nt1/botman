<?php

declare(strict_types=1);

use Migrations\AbstractMigration;

class AddForeignKeyForAll extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        //department
        $table = $this->table('department');
        $table->addForeignKey('department_leader_id', 'user', 'id');
        $table->save();
    }
}

<?php

declare(strict_types=1);

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateUser extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user', [
            'id' => false,
            'primary_key' => 'id',
            'encoding' => 'utf8',
            'collation' => 'utf8_general_ci'
        ])
            ->addColumn('id', 'biginteger', [
                'identity' => true
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('password', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('department_id', 'biginteger', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('entered_date', 'date', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('position_id', 'integer', [
                'default' => null,
                'limit' => MysqlAdapter::INT_TINY,
                'null' => false,
            ])
            ->addColumn('created_date', 'date', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('updated_date', 'date', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('deleted_date', 'date', [
                'default' => null,
                'null' => true,
            ]);
        $table->create();
    }
}

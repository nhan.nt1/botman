<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateDepartment extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('department', [
            'id' => false,
            'primary_key' => 'id',
            'encoding' => 'utf8',
            'collation' => 'utf8_general_ci'
        ])
            ->addColumn('id', 'biginteger', [
                'identity' => true
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('note', 'text', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('department_leader_id', 'biginteger', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('department_floor_num', 'integer', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('created_date', 'date', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('updated_date', 'date', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('deleted_date', 'date', [
                'default' => null,
                'null' => true,
            ]);
        $table->create();
    }
}

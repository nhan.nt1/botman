<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $this->fetch('title'); ?></title>
    <!-- Fontawesome -->
    <link href="<?= $this->Url->css('library/fontawesome-free/css/all.min.css') ?>" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href="<?= $this->Url->css('library/bootstrap/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= $this->Url->css('library/jquery-ui.min.css') ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= $this->Url->css('common') ?>" rel="stylesheet">
    <?= $this->fetch('css') ?>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <div class="sidebar">
                <?= $this->element('admin/layout/header') ?>
                <?= $this->element('admin/layout/sidebar') ?>
            </div>
        </aside>
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
        <?= $this->element('admin/layout/footer') ?>
    </div>
    <!-- Core JS -->
    <script type="text/javascript" src="<?= $this->Url->script('library/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('library/bootstrap/bootstrap.bundle.min.js') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('library/bootstrap/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('library/datepicker.js') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('library/moment.js') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('library/jquery-validation/jquery.validate.js') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('library/jquery-validation/additional-methods') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('library/jquery-validation/additional-setting') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('library/jquery-ui.min.js') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('common') ?>"></script>
    <?= $this->fetch('script') ?>
</body>

</html>
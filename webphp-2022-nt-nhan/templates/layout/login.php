<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link href="<?= $this->Url->css('library/fontawesome-free/css/all.min.css') ?>" rel="stylesheet">
    <link href="<?= $this->Url->css('library/bootstrap/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= $this->Url->css('common') ?>" rel="stylesheet">
    <?= $this->fetch('css') ?>
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="card">
            <div class="card-body login-card-body">
                <?= $this->fetch('content') ?>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?= $this->Url->script('library/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('library/bootstrap/bootstrap.bundle.min.js') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('library/bootstrap/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('library/jquery-validation/jquery.validate.js') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('library/jquery-validation/additional-methods') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('library/jquery-validation/additional-setting') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('common') ?>"></script>
    <?= $this->fetch('script') ?>
</body>

</html>
<?= $this->Flash->render() ?>
<?= $this->Form->create(null, ['id' => 'formLogin', 'data-submiting' => false]) ?>
<div class="">
    <div class="form-group">
        <label for="">Email:</label>
        <?= $this->Form->input(
            'email',
            [
                'type' => 'text',
                'class' => 'form-control',
                'label' => false,
                'id' => 'email',
                'data-label' => "Email",
                'required' => 'false'
            ]
        );
        ?>
    </div>
</div>
<div class="mb-3">
    <div class="form-group">
        <label for="">Password:</label>
        <?= $this->Form->input(
            'password',
            [
                'type' => 'password',
                'class' => 'form-control',
                'label' => false,
                'id' => 'password',
                'data-label' => "Password",
                'required' => 'false'
            ]
        );
        ?>
    </div>
</div>
<div class="row">
    <div class="col-4">
        <?= $this->Form->button('Login', [
            'class' => 'btn btn-primary btn-block btn-login',
        ]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
<?= $this->Html->script('admin/log/login.js', ['block' => true]) ?>
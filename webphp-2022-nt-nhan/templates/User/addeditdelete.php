<?php $this->assign('title', "UserAddEditDelete"); ?>
<?php
$url = isset($user->id) ? ['controller' => 'User', 'action' => 'edit', $user->id] : ['controller' => 'User', 'action' => 'add'];
?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="float-sm-right">
                        <label class="mr-4"><?= h($name_auth) ?></label>
                        <?= $this->Html->link('Logout', ['controller' => '', 'action' => 'logout']) ?>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <?= $this->Form->create($user, ['url' => $url, 'id' => 'formAddEditDeleteUser', 'novalidate' => true]) ?>
            <div class="container gap-3">
                <?= $this->Flash->render() ?>
                <div class="error-message"></div>
                <div class="row p-4">
                    <div class="col-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label style="width: 200px;" class="input-group-text">ID</label>
                            </div>
                            <div class="input-group-prepend">
                                <label style="width: 100px;" class="text-center">
                                    <?= isset($user->id) ? $user->id : '' ?>
                                </label>
                                <input type="hidden" id="ip_id_user" value=<?= isset($user->id) ? $user->id : '' ?>>
                                <input type="hidden" id="ip_id_auth" value=<?= isset($id_auth) ? $id_auth : '' ?>>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label style="width: 200px;" class="input-group-text">User Name</label>
                            </div>
                            <?= $this->Form->input(
                                'name',
                                [
                                    'style' => 'width: 150px;',
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'label' => false,
                                    'data-label' => "User Name",
                                    'required' => 'false',
                                    'maxlength' => 'false',
                                    'value' => isset($user->name) ? $user->name : '',
                                    'disabled' => $roleNameAuth === 'General Director' ? false : true
                                ]
                            );
                            ?>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="row p-4">
                    <div class="col-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label style="width: 200px;" class="input-group-text">Email</label>
                            </div>
                            <?= $this->Form->input(
                                'email',
                                [
                                    'style' => 'width: 180px;',
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'label' => false,
                                    'data-label' => "Email",
                                    'required' => 'false',
                                    'maxlength' => 'false',
                                    'value' => isset($user->email) ? $user->email : '',
                                    'disabled' => $roleNameAuth === 'General Director' ? false : true
                                ]
                            );
                            ?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label style="width: 200px;" class="input-group-text">Department</label>
                            </div>
                            <?= $this->Form->select(
                                'department_id',
                                $departments,
                                [
                                    'style' => 'width: 150px;',
                                    'class' => 'form-control',
                                    'label' => false,
                                    'empty' => '-',
                                    'id' => 'department_id',
                                    'data-label' => "Department",
                                    'required' => 'false',
                                    'disabled' => $roleNameAuth === 'General Director' ? false : true
                                ]
                            );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row p-4">
                    <div class="col-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label style="width: 200px;" class="input-group-text">Entered Date</label>
                            </div>
                            <?= $this->Form->input(
                                'entered_date',
                                [
                                    'id' => 'entered_date',
                                    'style' => 'width: 150px;',
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'label' => false,
                                    'data-label' => "Entered Date",
                                    'required' => 'false',
                                    'value' => isset($user->entered_date) ? date("Y/m/d", strtotime($user->entered_date)) : '',
                                    'disabled' => $roleNameAuth === 'General Director' ? false : true
                                ]
                            );
                            ?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="input-group">
                            <label style="width: 200px;" class="input-group-text">Position</label>
                            <?= $this->Form->select(
                                'position_id',
                                \App\Libs\ConfigUtil::get('role'),
                                [
                                    'style' => 'width: 150px;',
                                    'empty' => '-',
                                    'class' => 'form-control',
                                    'label' => false,
                                    'id' => 'position_id',
                                    'data-label' => "Position",
                                    'required' => 'false',
                                    'disabled' => $roleNameAuth === 'General Director' ? false : true
                                ]
                            );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row p-4">
                    <div class="col-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label style="width: 200px;" class="input-group-text">Password</label>
                            </div>
                            <?= $this->Form->input(
                                'password',
                                [
                                    'style' => 'width: 150px;',
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'maxlength' => 'false',
                                    'label' => false,
                                    'id' => 'password',
                                    'data-label' => "Password",
                                    'required' => 'false',
                                    'value' => ''
                                ]
                            );
                            ?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label style="width: 200px;" class="input-group-text">Password Confirmation</label>
                            </div>
                            <?= $this->Form->input(
                                'password_confirmation',
                                [
                                    'id' => 'password_confirmation',
                                    'style' => 'width: 150px;',
                                    'maxlength' => 'false',
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'label' => false,
                                    'data-label' => "Password Confirmation",
                                    'required' => 'false',
                                ]
                            );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row p-4">
                    <div class="col-2">
                        <?= $this->Form->button('Register', [
                            'id' => 'btn-save-user',
                            'class' => 'btn btn-info btn-block btn-save-user',
                            'type' => 'submit',
                            'style' => isset($user->id) ? 'display : none' : ''
                        ]) ?>
                    </div>
                    <div class="col-2">
                        <?= $this->Form->button('Update', [
                            'id' => 'btn-save-user',
                            'class' => 'btn btn-primary btn-block btn-save-user',
                            'type' => 'submit',
                            'style' => !isset($user->id) ? 'display : none' : ''
                        ]) ?>
                    </div>
                    <div class="col-2">
                        <?= $this->Form->button('Delete', [
                            'class' => 'btn btn-success btn-block btn-delete-user',
                            'type' => 'button',
                            'style' => !isset($user->id) ? 'display : none' : '',
                        ]) ?>
                    </div>
                    <div class="col-2">
                        <?= $this->Form->button('Cancel', [
                            'class' => 'btn btn-danger btn-block',
                            'type' => 'button',
                            'onclick = window.location.href' => $this->Url->build([
                                'controller' => 'User',
                                'action' => 'list',
                            ]),
                        ]) ?>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </section>
</div>
<?= $this->Form->create(null, ['url' => ['controller' => 'User', 'action' => 'delete', $user->id], 'id' => 'formDeleteUser']) ?>
<?= $this->Form->end() ?>
<?= $this->Html->script('admin/user/addeditdelete.js', ['block' => true]) ?>
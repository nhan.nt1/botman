<?php $this->assign('title', "User List"); ?>
<?php
$roleList = \App\Libs\ConfigUtil::get('role');
?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="float-sm-right">
                        <label class="mr-4"><?= h($name_auth) ?></label>
                        <?= $this->Html->link('Logout', ['controller' => '', 'action' => 'logout']) ?>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <?= $this->Form->create(null, ['id' => 'formSearchUser', 'type' => 'get']) ?>
            <div class="row">
                <div class="col-8">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text">User Name</label>
                        </div>
                        <div>
                            <?= $this->Form->input(
                                'name',
                                [
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'label' => false,
                                    'id' => 'name',
                                    'data-label' => "Name",
                                    'required' => 'false',
                                    'maxlength' => '50',
                                    'value' => !empty($searchData['name']) ? h($searchData['name']) : ''
                                ]
                            );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-5">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Entered Date From</label>
                        </div>
                        <?= $this->Form->input(
                            'entered_date_from',
                            [
                                'style' => "width: 151px;",
                                'type' => 'text',
                                'class' => 'form-control',
                                'label' => false,
                                'id' => 'entered_date_from',
                                'data-name' => "Entered Date From",
                                'required' => 'false',
                                'value' => !empty($searchData['entered_date_from']) ? h($searchData['entered_date_from']) : ''
                            ]
                        );
                        ?>
                    </div>
                </div>
                <div class="col-5">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Entered Date To</label>
                        </div>
                        <?= $this->Form->input(
                            'entered_date_to',
                            [
                                'type' => 'text',
                                'class' => 'form-control',
                                'label' => false,
                                'id' => 'entered_date_to',
                                'data-name' => "Entered Date To",
                                'required' => 'false',
                                'value' => !empty($searchData['entered_date_to']) ? h($searchData['entered_date_to']) : ''
                            ]
                        );
                        ?>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-4"></div>
                <div class="col-4">
                    <?= $this->Form->button('Clear', [
                        'class' => 'btn btn-primary ml-2 btnClear ',
                        'type' => 'button'
                    ]) ?>
                    <?= $this->Form->button('Search', [
                        'id' => 'search',
                        'class' => 'btn btn-success btn-search-user',
                        'type' => 'submit',
                    ]) ?>
                </div>
            </div>
            <?= $this->Form->end() ?>
            <div class="row mt-4">
                <div class="col-4"></div>
                <div class="col-4">
                    <?php if (count($users) > 0) : ?>
                        <div class="row row0">
                            <div class="col-sm-6 col-sm-pull-6 text-center text-sm-left">
                            </div>
                            <div class="col-sm-6 col-sm-push-6 text-center text-right">
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mb15 mt0 bb-pagination pr0 justify-content-end">
                                        <?= $this->Paginator->prev(__('前へ')) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next(__('次へ')) ?>
                                        <?= $this->Paginator->last(__('最終')) ?>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <?php if (isset($searchData)) : ?>
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>User Name</th>
                                            <th>Email</th>
                                            <th>Department Name</th>
                                            <th>Entered Date</th>
                                            <th>Position</th>
                                        </tr>
                                    </thead>

                                    <?php if (count($users) > 0) : ?>
                                        <tbody>
                                            <?php foreach ($users as $user) : ?>
                                                <tr>
                                                    <?php if ($roleNameAuth === 'General Director') : ?>
                                                        <td><?= $this->Html->link(h($user->name), ['action' => 'addeditdelete', $user->id]) ?></td>
                                                    <?php else : ?>
                                                        <td><?= h($user->name) ?></td>
                                                    <?php endif; ?>
                                                    <td><?= h($user->email) ?></td>
                                                    <td><?= $user->department ? h($user->department->name) : '' ?></td>
                                                    <td><?= h(date('Y/m/d', strtotime($user->entered_date))); ?></td>
                                                    <td><?= isset($roleList[$user->position_id]) ? h($roleList[$user->position_id]) : '' ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    <?php endif; ?>
                                </table>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="text-center">
                        <?= $this->Flash->render() ?>
                    </div>
                </div>
            </div>
            <?php if ($roleNameAuth === 'General Director') : ?>
                <div class="row mt-4">
                    <div class="col-4">
                        <?= $this->Form->button('New', [
                            'class' => 'btn btn-primary ml-2 ',
                            'type' => 'button',
                            'onclick = window.location.href' => $this->Url->build([
                                'controller' => 'User',
                                'action' => 'addeditdelete',
                            ]),
                        ]) ?>
                        <?= $this->Form->button('Output CSV', [
                            'class' => 'btn btn-success ',
                            'type' => 'button',
                            'hidden' => count($users) < 1 ? 'hidden' : '',
                            'onclick = window.location.href' => $this->Url->build([
                                'controller' => 'User',
                                'action' => 'outputCSV',
                            ]),
                        ]) ?>
                    </div>
                </div>
            <?php endif; ?>
    </section>
</div>
<?= $this->Html->script('admin/user/list.js', ['block' => true]) ?>
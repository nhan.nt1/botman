<?php $this->assign('title', "Department List"); ?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="float-sm-right">
                        <label class="mr-4"><?= h($name_auth) ?></label>
                        <?= $this->Html->link('Logout', ['controller' => '', 'action' => 'logout']) ?>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <?= $this->Form->end() ?>
            <div class="row mt-4">
                <div class="col-4"></div>
                <div class="col-4">
                    <?php if (count($departments) > 0) : ?>
                        <div class="row row0">
                            <div class="col-sm-6 col-sm-pull-6 text-center text-sm-left">
                            </div>
                            <div class="col-sm-6 col-sm-push-6 text-center text-right">
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mb15 mt0 bb-pagination pr0 justify-content-end">
                                        <?= $this->Paginator->prev(__('前へ')) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next(__('次へ')) ?>
                                        <?= $this->Paginator->last(__('最終')) ?>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?= $this->Flash->render() ?>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Department Name</th>
                                        <th>Department Note</th>
                                        <th>Department Leader Doctor</th>
                                        <th>Floor Number</th>
                                        <th>Created Date</th>
                                        <th>Updated Date</th>
                                        <th>Deleted Date</th>
                                    </tr>
                                </thead>
                                <?php if (count($departments) > 0) : ?>
                                    <tbody>
                                        <?php foreach ($departments as $department) : ?>
                                            <tr>
                                                <td><?= h($department->id) ?></td>
                                                <td><?= h($department->name) ?></td>
                                                <td><?= h($department->note) ?></td>
                                                <td><?= isset($department->user->name) ? h($department->user->name) : '' ?></td>
                                                <td><?= h($department->department_floor_num) ?></td>
                                                <td><?= date('Y/m/d', strtotime($department->created_date)); ?></td>
                                                <td><?= date('Y/m/d', strtotime($department->updated_date)); ?></td>
                                                <td><?= isset($department->deleted_date) ? date('Y/m/d', strtotime($department->deleted_date)) : ''; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                <?php endif ?>
                            </table>
                            <?php if (count($departments) < 1) : ?>
                                <div class='text-center error-message'>No Record</div>
                            <?php endif ?>
                        </div>
                    </div>
                    <?= $this->Form->button('Import CSV', [
                        'class' => 'btn btn-danger',
                        'type' => 'button',
                        'data-toggle' => "modal",
                        'data-target' => "#modelImportcsv"
                    ]) ?>
                </div>
            </div>
    </section>
</div>
<!-- Modal -->
<div class="modal fade" id="modelImportcsv" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?= $this->Form->create(null, ['id' => 'formImportCSV', 'type' => 'file', 'url' => ['controller' => 'Department', 'action' => 'list']]) ?>

            <div class="modal-header">
                <h5 class="modal-title">Import CSV</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <?= $this->Form->input(
                        'fileCSV',
                        [
                            'type' => 'file',
                            'class' => 'form-control-file',
                            'label' => false,
                            'data-label' => "fileCSV",
                        ]
                    );
                    ?>
                </div>
            </div>
            <div class="modal-footer">
                <?= $this->Form->button('Close', [
                    'class' => 'btn btn-secondary',
                    'type' => 'button',
                    'data-dismiss' => "modal"
                ]) ?>
                <?= $this->Form->button('Import CSV', [
                    'class' => 'btn btn-success btn-importcsv',
                    'type' => 'submit',
                ]) ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<?= $this->Html->script('admin/department/list.js', ['block' => true]) ?>
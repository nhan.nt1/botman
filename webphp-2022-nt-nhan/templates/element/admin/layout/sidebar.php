<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
            <?= $this->Html->link(
                'User List',
                [
                    'controller' => 'user',
                    'action' => 'list',
                ],
                ['class' => 'nav-link', 'escape' => false]
            ); ?>
        </li>
        <?php if ($roleNameAuth === 'General Director') : ?>
            <li class="nav-item">
                <?= $this->Html->link(
                    'Department List',
                    [
                        'controller' => 'department',
                        'action' => 'list',
                    ],
                    ['class' => 'nav-link']
                ); ?>
            </li>
        <?php endif; ?>
</nav>
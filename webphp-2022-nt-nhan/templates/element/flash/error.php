<?php if (isset($params) and isset($params['errors'])) : ?>
        <?php foreach ($params['errors'] as $error) : ?>
            <div class="error-message" onclick="this.classList.add('hidden');"><?php echo h($error) ?></div>
        <?php endforeach; ?>
<?php endif; ?>

<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="error-message" onclick="this.classList.add('hidden');"><?php echo $message ?></div>
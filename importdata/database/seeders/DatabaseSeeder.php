<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Orders;
use App\Models\Order_details;
use Illuminate\Database\Seeder;
use Database\Seeders\ProductSeeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::truncate();
        User::factory(10000)->create();
        ////order
        // $this->call([
        //     ProductSeeder::class,
        // ]);
        // Orders::truncate();
        // Orders::factory(10000)->create();

        // Order_details::truncate();
        // Order_details::factory(100000)->create();
    }
}

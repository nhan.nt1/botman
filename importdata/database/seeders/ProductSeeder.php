<?php

namespace Database\Seeders;

use File;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::truncate();
        $json = File::get("database/data/product.json");
        $products = json_decode($json);
        foreach ($products as $key => $value) {

            if (isset($value->configurable_options[0])) {
                $configurable_options = json_encode($value->configurable_options[0]->values);
            }
            if (isset($value->quantity_sold)) {
                $quantity_sold = $value->quantity_sold->value;
            }
            if (isset($value->categories)) {
                $categories = json_encode($value->categories);
            }
            if (isset($value->breadcrumbs)) {
                $breadcrumbs = json_encode($value->breadcrumbs);
            }
            Product::create([
                "name" => $value->name,
                "brand" => json_encode($value->brand),
                "price" => $value->price,
                "original_price" => $value->original_price,
                "list_price" => $value->list_price,
                "short_description" => $value->short_description,
                "badges" => json_encode($value->badges),
                "badges_new" => json_encode($value->badges_new),
                "discount" => $value->discount,
                "discount_rate" => $value->discount_rate,
                "productset_group_name" => $value->productset_group_name,
                "rating_average" => $value->rating_average,
                "review_count" => $value->review_count,
                "thumbnail_url" => $value->thumbnail_url,
                "configurable_options" => $configurable_options,
                "inventory_status" => $value->inventory_status,
                // "stock_item" => json_encode($value->stock_item),
                "quantity_sold" => $quantity_sold,
                "categories" => $categories,
                "breadcrumbs" => $breadcrumbs,
            ]);
        }
    }
}

<?php

namespace Database\Factories;

use App\Models\Orders;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Orders>
 */
class OrdersFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $dateTime =  \Carbon\Carbon::createFromTimeStamp($this->faker->dateTimeBetween('-4 years' ,'now')->getTimestamp());
        return [
            'cusomter_id' => rand(1,10000),
            'created_at' => $dateTime,
            'updated_at' => $dateTime       
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\Orders;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order_details>
 */
class Order_detailsFactory extends Factory
{
    public $rd = 2;
    public $orderId = 1;
    public $count = 0;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $products = Product::all();
        $id = rand(1, count($products));
        if (isset($products[$id])) {

            $colors = json_decode($products[$id]['configurable_options']);
            $countColor = count($colors);
            $rdColor = rand(0, $countColor - 1);
            $this->count++;

            if ($this->count > $this->rd) {
                $this->rd = rand(1, 20);
                $this->count = 0;
                $this->orderId++;
            }

            return [
                'order_id' => $this->orderId,
                'product_id' => $products[$id],
                'color' => $rdColor,
                'amout' => rand(1, 6),
                'price' => $products[$id]['price'],
                'rating' => rand(3, 5),
                'created_at' => null,
                'updated_at' => null,
            ];
        }
        return [
            'order_id' => null, 
            'product_id' => null,
            'color' => null,
            'amout' => null,
            'price' => null,
            'rating' => null,
            'created_at' => null,
            'updated_at' => null,
        ];
    }
}

<?php

namespace Database\Seeders;

use File;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::truncate();
        $json = File::get("database/data/product.json");
        $todos = json_decode($json);

        foreach ($todos as $key => $value) {
            Product::create([
                "id" => $value->id,
                "name" => $value->name,
                "brand" => json_encode($value->brand),
                "price" => $value->price,
                "original_price" => $value->original_price,
                "list_price" => $value->list_price,
                "short_description" => $value->short_description,
                "badges" => json_encode($value->badges),
                "badges_new" => json_encode($value->badges_new),
                "discount" => $value->discount,
                "discount_rate" => $value->discount_rate,
                "productset_group_name" => $value->productset_group_name,
                "rating_average" => $value->rating_average,
                "review_count" => $value->review_count,
                "thumbnail_url" => $value->thumbnail_url,
                "configurable_options" => json_encode($value->configurable_options),
                // "inventory_status" => $value->inventory_status,
                // "stock_item" => $value->stock_item,
                // "quantity_sold" => $value->quantity_sold,
                // "categories" => $value->categories,
                // "breadcrumbs" => $value->breadcrumbs,
            ]);
        }
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->string('id');
            $table->text('name')->nullable();
            $table->json('brand')->nullable();
            $table->text('price')->nullable();
            $table->text('original_price')->nullable();
            $table->text('list_price')->nullable();
            $table->text('short_description')->nullable();
            $table->json('badges')->nullable();
            $table->json('badges_new')->nullable();
            $table->text('discount')->nullable();
            $table->text('discount_rate')->nullable();
            $table->text('productset_group_name')->nullable();
            $table->text('rating_average')->nullable();
            $table->text('review_count')->nullable();
            $table->text('thumbnail_url')->nullable();
            $table->json('configurable_options')->nullable();
            $table->text('inventory_status')->nullable();
            $table->json('stock_item')->nullable();
            $table->json('quantity_sold')->nullable();
            $table->json('categories')->nullable();
            $table->json('breadcrumbs')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

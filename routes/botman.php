<?php

use App\Http\Controllers\BotManController;
use App\Conversations\OnboardingConversation;
use BotMan\Drivers\Facebook\Extensions\Element;
use BotMan\Drivers\Facebook\Interfaces\Airline;
use BotMan\Drivers\Facebook\Extensions\ListTemplate;
use BotMan\Drivers\Facebook\Extensions\ElementButton;
use BotMan\Drivers\Facebook\Extensions\MediaTemplate;
use BotMan\Drivers\Facebook\Extensions\ButtonTemplate;
use BotMan\Drivers\Facebook\Extensions\ReceiptAddress;
use BotMan\Drivers\Facebook\Extensions\ReceiptElement;
use BotMan\Drivers\Facebook\Extensions\ReceiptSummary;
use BotMan\Drivers\Facebook\Extensions\GenericTemplate;
use BotMan\Drivers\Facebook\Extensions\MediaUrlElement;
use BotMan\Drivers\Facebook\Extensions\ReceiptTemplate;
use BotMan\Drivers\Facebook\Extensions\OpenGraphElement;
use BotMan\Drivers\Facebook\Extensions\OpenGraphTemplate;
use BotMan\Drivers\Facebook\Extensions\ReceiptAdjustment;
use BotMan\Drivers\Facebook\Extensions\AirlineUpdateTemplate;
use BotMan\Drivers\Facebook\Extensions\Airline\AirlineAirport;
use BotMan\Drivers\Facebook\Extensions\AirlineCheckInTemplate;
use BotMan\Drivers\Facebook\Extensions\MediaAttachmentElement;
use BotMan\Drivers\Facebook\Extensions\AirlineItineraryTemplate;
use BotMan\Drivers\Facebook\Extensions\Airline\AirlineFlightInfo;
use BotMan\Drivers\Facebook\Extensions\Airline\AirlineBoardingPass;
use BotMan\Drivers\Facebook\Extensions\AirlineBoardingPassTemplate;
use BotMan\Drivers\Facebook\Extensions\Airline\AirlinePassengerInfo;
use BotMan\Drivers\Facebook\Extensions\Airline\AirlineFlightSchedule;
use BotMan\Drivers\Facebook\Extensions\Airline\AirlineExtendedFlightInfo;
use BotMan\Drivers\Facebook\Extensions\Airline\AirlinePassengerSegmentInfo;

$botman = resolve('botman');

$botman->hears('/start|GET_STARTED', function ($bot) {
    $bot->reply('Hello bro');
}); 

$botman->hears('{message}', function ($bot) {
    $bot->startConversation(new OnboardingConversation);
});



$botman->hears('Start conversation', BotManController::class . '@startConversation');

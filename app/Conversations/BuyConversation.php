<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\Drivers\Facebook\Extensions\Element;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\Drivers\Facebook\Extensions\ElementButton;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\Drivers\Facebook\Extensions\ReceiptAddress;
use BotMan\Drivers\Facebook\Extensions\ReceiptElement;
use BotMan\Drivers\Facebook\Extensions\ReceiptSummary;
use BotMan\Drivers\Facebook\Extensions\GenericTemplate;
use BotMan\Drivers\Facebook\Extensions\ReceiptTemplate;
use BotMan\Drivers\Facebook\Extensions\ReceiptAdjustment;

class BuyConversation extends Conversation
{
    /**
     * First question
     */
    protected $product = [];
    protected $nameProduct = '';
    protected $amountProduct = '';
    protected $nameConsignee;
    protected $addressConsignee;
    protected $phoneConsignee;
    protected $paymentMethod;

    public function askNameProduct()
    {
        $this->ask('Vui lòng nhập tên phẩm bạn muốn mua ', function (Answer $answer) {
            // Save result
            $this->nameProduct = $answer->getText();
            $this->askAmountProduct();
        });
    }
    public function askAmountProduct()
    {
        $this->ask('Nhập số lượng bạn muốn mua', function (Answer $answer) {
            $this->amountProduct = $answer->getText();
            $this->product[] = ['name' => $this->nameProduct, 'amount' => $this->amountProduct];
            $this->askAgain();
        });
    }
    public function askAgain()
    {
        $question = Question::create("Bạn có muốn mua gì nữa không?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create('Có')->value('yes'),
                Button::create('Không')->value('no'),
            ]);
        $this->ask($question, function (Answer $answer) {
            // Save result
            if ($answer->isInteractiveMessageReply()) {
                switch ($answer->getValue()) {
                    case 'yes':
                        $this->askNameProduct();
                        break;
                    case 'no':
                        $this->bot->reply('bạn đã hoàn thành giỏ hàng với length là ' . count($this->product));
                        //ask name after installed cart
                        $this->askNameConsignee();
                        break;

                    default:
                        break;
                }
            }
        });
    }
    public function askNameConsignee()
    {
        $this->ask('Cho mình xin tên người nhận hàng ?', function (Answer $answer) {
            // Save result
            $this->nameConsignee = $answer->getText();
            $this->askPhoneConsignee();
        });
    }
    public function askPhoneConsignee()
    {
        $this->ask('Cho mình xin số điện thoại người nhận hàng ạ', function (Answer $answer) {
            // Save result
            $this->phoneConsignee = $answer->getText();

            $this->askAddressConsignee();
        });
    }
    public function askAddressConsignee()
    {
        $this->ask('Cho mình xin địa chỉ người nhận hàng ạ', function (Answer $answer) {
            // Save result
            $this->addressConsignee = $answer->getText();
            $this->askPayment();
        });
    }
    public function askPayment()
    {
        $question = Question::create("Bạn muốn chọn phương thức thanh toán là gì ?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create('Nhận hàng trực tiếp')->value('paymentOnDelivery'),
                Button::create('Paypal')->value('paypalPayment'),
            ]);
        $this->ask($question, function (Answer $answer) {
            // Save result
            if ($answer->isInteractiveMessageReply()) {
                $this->paymentMethod = $answer->getValue();
                // prinf bill
                $this->prinfBill();
            }
        });
    }
    public function prinfBill()
    {
        $this->bot->reply(
            ReceiptTemplate::create()
                ->recipientName('Christoph Rumpel')
                ->merchantName('BotMan GmbH')
                ->orderNumber('342343434343')
                ->timestamp('1428444852')
                ->orderUrl('http://test.at')
                ->currency('USD')
                ->paymentMethod('VISA')
                ->addElement(
                    ReceiptElement::create('T-Shirt Small')
                        ->price(15.99)
                        ->image('http://botman.io/img/botman-body.png')
                )
                ->addElement(
                    ReceiptElement::create('Sticker')
                        ->price(2.99)
                        ->image('http://botman.io/img/botman-body.png')
                )
                ->addAddress(
                    ReceiptAddress::create()
                        ->street1('Watsonstreet 12')
                        ->city('Bot City')
                        ->postalCode(100000)
                        ->state('Washington AI')
                        ->country('Botmanland')
                )
                ->addSummary(
                    ReceiptSummary::create()
                        ->subtotal(18.98)
                        ->shippingCost(10)
                        ->totalTax(15)
                        ->totalCost(23.98)
                )
                ->addAdjustment(
                    ReceiptAdjustment::create('Laravel Bonus')
                        ->amount(5)
                )
        );
    }

    /**
     * Start the conversation
     */
    public function run()
    {
        $this->askNameProduct();
    }
}

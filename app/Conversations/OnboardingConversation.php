<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use App\Conversations\AdviseConversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\Drivers\Facebook\Extensions\ReceiptAddress;
use BotMan\Drivers\Facebook\Extensions\ReceiptElement;
use BotMan\Drivers\Facebook\Extensions\ReceiptSummary;
use BotMan\Drivers\Facebook\Extensions\ReceiptTemplate;
use BotMan\Drivers\Facebook\Extensions\ReceiptAdjustment;

class OnboardingConversation extends Conversation
{
    protected $firstname;

    protected $email;

    public function askForDatabase()
    {
        $question = Question::create('What do you need?')
            ->fallback('Unable')
            ->callbackId('doyouknow')
            ->addButtons([
                Button::create('Tư vấn đồ sơ mi')->value('advise'),
                Button::create('Đặt hàng')->value('buy'),
                Button::create('Liên hệ nhân viên')->value('chatWithStaff'),
            ]);
        $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                $selectedValue = $answer->getValue();
                switch ($selectedValue) {
                    case 'advise':
                        $this->bot->startConversation(new AdviseConversation);
                        break;
                    case 'buy':
                        $this->bot->startConversation(new BuyConversation);
                        break;

                    default:
                        # code...
                        break;
                }
            } else {
                $this->say('Chúng tôi không hiểu bạn đang muốn gì?');
                $this->askForDatabase();
            }
        });
        // $this->bot->reply(
        //     ReceiptTemplate::create()
        //         ->recipientName('Thành nhân')
        //         ->orderNumber('số đơn hàng')
        //         ->timestamp('1428444852')
        //         ->currency('VND')
        //         ->paymentMethod('Thanh toán trực tiếp')
        //         ->addElement(
        //             ReceiptElement::create('T-Shirt Small')
        //                 ->price(15.99)
        //                 ->image('http://botman.io/img/botman-body.png')
        //         )
        //         ->addElement(
        //             ReceiptElement::create('Sticker')
        //                 ->price(2.99)
        //                 ->image('http://botman.io/img/botman-body.png')
        //         )
        //         ->addAddress(
        //             ReceiptAddress::create()
        //                 ->street1('c23/2, tổ 9, khu phố 2, tăng nhơn phú A, q9, HCM')
        //                 ->city('VN')
        //                 ->postalCode(+84)
        //                 ->state('0337376811 +')
        //                 ->country('Botmanland')
        //         )
        //         ->addSummary(
        //             ReceiptSummary::create()
        //                 ->subtotal(18.98)
        //                 ->shippingCost(10)
        //                 ->totalTax(15)
        //                 ->totalCost(23.98)
        //         )
        //         ->addAdjustment(
        //             ReceiptAdjustment::create('Laravel Bonus')
        //                 ->amount(5)
        //         )
        // );
    }

    public function run()
    {
        // This will be called immediately
        $this->askForDatabase();
    }
}

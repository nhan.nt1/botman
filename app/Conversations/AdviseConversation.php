<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\Drivers\Facebook\Extensions\Element;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\Drivers\Facebook\Extensions\ElementButton;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\Drivers\Facebook\Extensions\GenericTemplate;

class AdviseConversation extends Conversation
{
    /**
     * First question
     */
    protected $height;
    protected $weight;
    protected $sex;
    protected $size = '';


    public function askSex()
    {
        $question = Question::create("Giới tính mà bạn muốn tư vấn là gì?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create('Nam')->value('male'),
                Button::create('Nữ')->value('female'),
            ]);
        $this->ask($question, function (Answer $answer) {
            // Save result
            if ($answer->isInteractiveMessageReply()) {
                $this->sex = $answer->getValue();
            }
            $this->askHeight();
        });
    }
    public function askHeight()
    {
        $this->ask('Vui lòng nhập chiều cao của bạn (ví dụ cao 1m7 thì nhập 170)', function (Answer $answer) {
            // Save result
            $this->height = $answer->getText();
            $this->askWeight();
        });
    }
    public function askWeight()
    {
        $this->ask('Vui lòng nhập cân nặng của bạn (ví dụ nặng 50kg thì nhập 50)', function (Answer $answer) {
            // Save result
            $this->weight = $answer->getText();
            $this->size();
        });
    }
    public function size()
    {
        switch ($this->sex) {
            case 'male':
                $sizeMale = [
                    ['height' => [160, 165], 'weight' => [55, 60], 'size' => 'S'],
                    ['height' => [164, 169], 'weight' => [60, 65], 'size' => 'M'],
                    ['height' => [170, 174], 'weight' => [66, 70], 'size' => 'L'],
                    ['height' => [174, 176], 'weight' => [70, 76], 'size' => 'Xl'],
                    ['height' => [175, 177], 'weight' => [76, 80], 'size' => 'XXL'],
                ];
                $this->findSize($sizeMale);
                break;
            case 'female':
                $sizeFemale = [
                    ['height' => [148, 153], 'weight' => [38, 43], 'size' => 'S'],
                    ['height' => [153, 155], 'weight' => [43, 46], 'size' => 'M'],
                    ['height' => [153, 158], 'weight' => [46, 53], 'size' => 'L'],
                    ['height' => [155, 162], 'weight' => [53, 57], 'size' => 'Xl'],
                    ['height' => [155, 166], 'weight' => [57, 66], 'size' => 'XXL'],
                ];
                $this->findSize($sizeFemale);
                break;

            default:

                break;
        }
        if ($this->size !== '') {
            $this->say('Bạn nên mặc đồ size ' . $this->size . ' điều này sẽ phù hợp với bạn');
        }
        $this->say('Dưới này là 1 số sản phẩm của shop và có thể sẽ phù hợp với bạn');
        $this->bot->reply(
            GenericTemplate::create()
                ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
                ->addElements([
                    Element::create('BotMan Documentation')
                        ->subtitle('All about BotMan')
                        ->image('https://thuviendohoa.vn/upload/images/items/nhan-vat-shin-cau-be-but-chi-hinh-anh-png-620.jpg')
                        ->addButton(
                            ElementButton::create('visit')
                                ->url('http://botman.io')
                        )
                        ->addButton(
                            ElementButton::create('tell me more')
                                ->payload('tellmemore')
                                ->type('postback')
                        ),
                    Element::create('BotMan Laravel Starter')
                        ->subtitle('This is the best way to start with Laravel and BotMan')
                        ->image('https://thuviendohoa.vn/upload/images/items/nhan-vat-shin-cau-be-but-chi-hinh-anh-png-620.jpg')
                        ->addButton(
                            ElementButton::create('visit')
                                ->url('https://github.com/mpociot/botman-laravel-starter')
                        ),
                ])
        );
    }
    public function findSize($arr)
    {
        foreach ($arr as $value) {
            if ($value['height'][0] <= $this->height &&  $this->height <= $value['height'][1]) {
                if ($value['weight'][0] <= $this->weight && $this->weight <= $value['weight'][1]) {
                    $this->size = $value['size'];
                }
            }
        }
    }
    /**
     * Start the conversation
     */
    public function run()
    {
        $this->askSex();
    }
}
